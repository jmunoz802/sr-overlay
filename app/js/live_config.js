/*
Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

    http://aws.amazon.com/apache2.0/

or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
*/

$(document).ready(function(){

  var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
      var anHttpRequest = new XMLHttpRequest();
      anHttpRequest.onreadystatechange = function() {
        if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
          aCallback(anHttpRequest.responseText);
      }

      anHttpRequest.open( "GET", aUrl, true );
      // anHttpRequest.setRequestHeader("User-Agent", "Jiffy-Overlay-0.0.1");
      anHttpRequest.send( null );
    }

    this.getSync = function(aUrl, aCallback) {
      var anHttpRequest = new XMLHttpRequest();
      anHttpRequest.onreadystatechange = function() {
        if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
          aCallback(anHttpRequest.responseText);
      }

      anHttpRequest.open( "GET", aUrl, false );
      // anHttpRequest.setRequestHeader("User-Agent", "Jiffy-Overlay-0.0.1");
      anHttpRequest.send( null );
    }
  }

  class JiffyBroadcaster {

    constructor() {
      var data = true;

      this.srPath = "https://www.speedrun.com/api/v1/";
      this.channelPath = "https://api.twitch.tv/kraken/channels/";
      this.streamPath = "https://api.twitch.tv/kraken/streams/";
      // this.helixstreamPath = "https://api.twitch.tv/helix/streams/";
      this.client = new HttpClient();
      this.viewerInfo = JSON.parse('{"uniqueId":0, "runs":{"0":"", "1":"", "2":""},"subcategories":[]}');

      if(!localStorage.getItem("jiffy-data")){
        this.info = JSON.parse('{"channelId":"" , "username":"" , "userId":"" , "categoryId":"", "levelId":"", "subcategories":[], "game":"", "advanced":false, "individualLevels":false}');
      } else {
        this.info = JSON.parse(localStorage.getItem("jiffy-data"));
      }

      if(this.readyToPollSr()){
        repopulateGameInfo(this);

        if(!this.info.noSrl){
          this.startPollSr();
        }
      }

      if(!this.info.advanced){
        this.startPollTwitch();
      }

      this.startSend();
    }

    setChannelId(id){
      if(!this.info.channelId){
        this.info.channelId = id;

        if(!this.info.game && !this.info.advanced) {
          pollGame(this);
        }
      }
    }

    /**
     * Conditions for saving data to localStorage and those error states
     *  for missing information
     */
    saveInfo(){
      var error = false;
      if($('#srUsername').hasClass("error") || $('#srUsername').val() == "") {
        //$('#srUsername').removeClass('flash');
        $('#srUsername').addClass('error flash');
        window.setTimeout(function(){$('#srUsername').removeClass('flash');}, 1000);
        error = true;
      }

      if(sr.info.advanced && $('#srGame').hasClass('error') || $('#srGame').val() == ""){
        //$('#srGame').removeClass('flash');
        $('#srGame').addClass('error flash');
        window.setTimeout(function(){$('#srGame').removeClass('flash');}, 1000);
        error = true;
      }

      if(sr.info.hasVariable){
        $('.choice-select').each(function(index){
          var option = $(this).find('option:selected').attr('value');

          if(!option){
            $(this).addClass('error flash');
            window.setTimeout(function(){$(this).removeClass('flash');}, 1000);
            error = true;
          }
        });
      }

      if(error){
        $('.save-area .error-x').removeClass('hidden');
        window.setTimeout(function(){$('.save-area .error-x').addClass('hidden');}, 1000);
        return;
      }

      localStorage.setItem("jiffy-data", JSON.stringify(this.info));

      $('.save-area .checkmark').removeClass('hidden');
      window.setTimeout(function(){$('.save-area .checkmark').addClass('hidden');}, 1000);

      if(this.readyToPollSr()) {
        this.startPollSr();
      }
    }

    /**
     * Functions dedicated to polling SRCom for leaderbaords/personal best
     */
    readyToPollSr(){
      var ready = (this.info.game != "") &&
                  (this.info.userId != "") &&
                  (this.info.categoryId != "");

      var subcatLength = Object.keys(this.info.subcategories).length;

      var varReady = (this.info.hasVariable && subcatLength > 0) ||
                     (!this.info.hasVariable && subcatLength == 0);

      if(ready && varReady){
        return true;
      }

      return false;
    }

    pausePollSr(){
      window.clearInterval(this.loopPollRecords);
      this.loopPollRecords = "";
    }

    startPollSr(){
      if(this.loopPollRecords){
        return;
      }

      if(!this.info.noSrl){
        this.loopPollRecords = window.setInterval(pollRecords, 5000, this);
      }
    }

    /**
     * Functions dedicated to polling twitch servers for Broadcaster's current
     *  game
     */
    pausePollTwitch(){
      window.clearInterval(this.loopPollGame);
      this.loopPollGame = "";
    }

    startPollTwitch(){
      if(this.loopPollGame){
        return;
      }

      if(!this.info.advanced){
        this.loopPollGame = window.setInterval(pollGame, 5000, this);
      }
    }

    /**
     * Functions dedicated to Sending Viewer Information
     */
    readyToSend(){
      var ready = this.viewerInfo.cover &&
        this.viewerInfo.name &&
        this.viewerInfo.weblink &&
        this.viewerInfo.runs &&
        this.viewerInfo.runnerRecord &&
        this.info.isLive;

      if(ready) {
        $('.offline').removeClass('active');
        $('.online').addClass('active');
        return ready;
      }

      $('.offline').addClass('active');
      $('.online').removeClass('active');
      return false;
    }

    pauseSend(){
      window.clearInterval(this.loopSend);
      this.loopSend = "";
    }

    startSend(){
      if(this.loopSend){
        return;
      }

      this.loopSend = window.setInterval(sendInfo, 5000, this);
    }

    /**
     * Updates uniqueId so viewer knows when to update leaderboard
     */
    updateUniqueId(){
      this.viewerInfo.uniqueId = (this.viewerInfo.uniqueId + 1) % 100;
    }
    
    populateVariables(){
      var selected = $("#srCategory option:selected"),
          categoryId = selected.attr("value"),
          categories = this.gameInfo.categories.data,
          ILs = false,
          variables = this.gameInfo.variables.data,
          levels = this.gameInfo.levels.data;

      this.info.categoryId = categoryId;
      this.info.subcategories = {};
      this.viewerInfo["category"] = selected.text();
      this.viewerInfo["weblink"] = selected.attr('data-weblink');
      this.viewerInfo["subcategories"] = [];

      for(var catIndx = 0; catIndx < categories.length; catIndx++) {
        var loopCat = categories[catIndx];
        if(loopCat.id == categoryId) {
          if( loopCat.type == "per-level" ) {
            ILs = true;
          }
          break;
        }
      }
  
      if(ILs){
        this.info.individualLevels = true;
        var firstFind = false,
            levelOptions = "",
            choiceCount = 0,
            totalChoices = "";

        for(var indx = 0; indx < levels.length; indx++){
          var currentLevel = levels[indx];
  
          if(!firstFind) {
            levelOptions += "<option selected value='" + currentLevel.id + "'>" + currentLevel.name + "</option>";
            this.info.levelId = currentLevel.id;
            this.viewerInfo.subcategories[choiceCount] = currentLevel.name;
            firstFind = true;
          } else {
            levelOptions += "<option value='" + currentLevel.id + "'>" + currentLevel.name + "</option>";
          }
        }

        $('.srLevels, .levels-label').removeClass('hidden');
        $('#srLevels').html(levelOptions);
        choiceCount++;
        sr.viewerInfo.hasVariable = true;
        
        for(var indx = 0; indx < variables.length; indx++){
          var currentVar = variables[indx];

          if( currentVar.category == categoryId && 
              currentVar.scope.type == "single-level" &&
              currentVar.scope.level == this.info.levelId && 
              currentVar["is-subcategory"] &&
              currentVar.mandatory){


            if(firstFind){
              this.info.hasVariable = true;
              this.viewerInfo["hasVariable"] = true;
              this.viewerInfo.runnerRecord = "none";
              firstFind = false;
            }
  
            totalChoices += "<label class='line-label choice-label' for='choice-" + choiceCount +"'> " + currentVar.name + "</label> \
                              <select id='choice-" + choiceCount + "' data-variable='" + currentVar.id + "' class='choice-select'>";
  
            totalChoices += "<option disabled>Select a subcategory</option>";
            for(var currentId in currentVar.values.values){
              var currentSubcategory = currentVar.values.values[currentId];

              if(currentVar.values.default == currentId) {
                totalChoices += "<option selected value='" + currentId + "'>" + currentSubcategory.label + "</option>";

                this.viewerInfo.subcategories[choiceCount] = currentSubcategory.label;
                this.info.subcategories[currentVar.id] = currentId;
              } else {
                totalChoices += "<option value='" + currentId + "'>" + currentSubcategory.label + "</option>";
              }
            }
  
            totalChoices += "</select>"
            choiceCount++;
          }
        }

        $('.choices').removeClass('hidden');
        $('.choices').html(totalChoices);
        hookSubcategories();

      } else {
        this.info.individualLevels = false;
        this.info.levelId = "";

        if(variables.length > 0){
          var totalChoices = "",
              choiceCount = 0,
              firstFind = true;
    
          for(var indx = 0; indx < variables.length; indx++){
            var currentVar = variables[indx];
            if( (currentVar.category == categoryId || !currentVar.category) && 
                currentVar["is-subcategory"] &&
                currentVar.mandatory){

              if(firstFind){
                this.info.hasVariable = true;
                this.viewerInfo["hasVariable"] = true;
                this.viewerInfo.runnerRecord = "none";
                firstFind = false;
              }
    
              totalChoices += "<label class='line-label choice-label' for='choice-" + indx +"'> " + currentVar.name + "</label> \
                                <select id='choice-" + indx + "' data-variable='" + currentVar.id + "' class='choice-select'>";
    
              totalChoices += "<option disabled>Select a subcategory</option>";
              for(var currentId in currentVar.values.values){
                var currentSubcategory = currentVar.values.values[currentId];
                
                if(currentVar.values.default == currentId) {
                  totalChoices += "<option selected value='" + currentId + "'>" + currentSubcategory.label + "</option>";

                  this.viewerInfo.subcategories[choiceCount] = currentSubcategory.label;
                  this.info.subcategories[currentVar.id] = currentId;

                } else {
                  totalChoices += "<option value='" + currentId + "'>" + currentSubcategory.label + "</option>";
                }
              }
    
              totalChoices += "</select>"
              choiceCount++;
            }
          }
    
          //No direct subcategory Found
          if(firstFind){
            this.viewerInfo["hasVariable"] = false;
          }
    
          $('.srLevels, .levels-label').addClass('hidden');
          $('.choices').removeClass('hidden');
          $('.choices').html(totalChoices);
          hookSubcategories();
    
        } else {
          this.info["hasVariable"] = false;
        }
      }
  
      if(sr.readyToPollSr()){
        //Set new UniqueId to inform client of new Info
        sr.updateUniqueId();

        sr.startPollSr();
      }
    }

    levelChange(){
      var selected = $("#srCategory option:selected"),
          levelSelected = $("#srLevels option:selected"),
          categoryId = selected.attr("value"),
          variables = this.gameInfo.variables.data,
          totalChoices = "";

      this.info.levelId = levelSelected.attr("value");
      this.viewerInfo.subcategories[0] = levelSelected.text();

      for(var indx = 0; indx < variables.length; indx++){
        var currentVar = variables[indx];

        if( currentVar.category == categoryId && 
            currentVar.scope.type == "single-level" &&
            currentVar.scope.level == this.info.levelId && 
            currentVar["is-subcategory"] &&
            currentVar.mandatory){

          if(firstFind){
            this.info.hasVariable = true;
            this.viewerInfo["hasVariable"] = true;
            this.viewerInfo.runnerRecord = "none";
            firstFind = false;
          }

          totalChoices += "<label class='line-label choice-label' for='choice-" + indx +"'> " + currentVar.name + "</label> \
                            <select id='choice-" + indx + "' data-variable='" + currentVar.id + "' class='choice-select'>";

          totalChoices += "<option disabled>Select a subcategory</option>";
          for(var currentId in currentVar.values.values){
            var currentSubcategory = currentVar.values.values[currentId];
            
            if(currentVar.values.default == currentId) {
              totalChoices += "<option selected value='" + currentId + "'>" + currentSubcategory.label + "</option>";

              this.viewerInfo.subcategories[choiceCount] = currentSubcategory.label;
              this.info.subcategories[currentId] = currentSubcategory;
            } else {
              totalChoices += "<option value='" + currentId + "'>" + currentSubcategory.label + "</option>";
            }
          }

          totalChoices += "</select>"
          choiceCount++;
        }
      }

      // $('.choices').removeClass('hidden');
      $('.choices').html(totalChoices);
      hookSubcategories();
    }

  }

  var sr = new JiffyBroadcaster();

  /**
   * Jquery callbacks for changes/clicks
   */
  $('.save-info').on('click', function(e){
    e.preventDefault();
    sr.saveInfo();
  });

  $('#srUsername').on('blur', function(e){
    var name = $('#srUsername').val(),
        lname = name.toLowerCase(),
        lusername = sr.info.username.toLowerCase();

    if(name == ""){
      $('#srUsername').next().removeClass('hidden');
      return;
    }
    if(lname != lusername) {
      sr.info.username = name;
      getRunnerId(sr);
    }
  });

  $('#srGame').on('blur', function(e){
    var name = $('#srGame').val();
    if(name == ""){
      $('#srGame').next().removeClass('hidden');
      return;
    }
    if(name != sr.info.game){
      sr.info.game = name;
      getGameInfo(sr);
    }
  });

  $('#srCategory').on('change', function(e){
    //If started Polling/No harm to repeat
    sr.pausePollSr();

    sr.populateVariables();

    if(sr.readyToPollSr()){
      sr.startPollSr();
    }

    //Set new UniqueId to inform client of new Info
    sr.updateUniqueId();
  });
  $('#srCategory').dropdown();

  $('#srLevels').on('change', function(e){
    sr.levelChange();

    //Set new UniqueId to inform client of new Info
    sr.updateUniqueId();
  });
  $('#srLevels').dropdown();

  $('.advanced').on('click', function(e){
    if( $('.advanced.slider').hasClass('active') ) {
      $('.slider').removeClass('active');
      $('#srGame').prop("disabled", "disabled");
      sr.info.advanced = false;
      sr.startPollTwitch();
    } else {
      $('.slider').addClass('active');
      $('#srGame').removeAttr('disabled');
      sr.info.advanced = true;
      sr.pausePollTwitch();
      // sr.info.isLive = true;
    }
  });

  /**
   * Set onAuthorize callback to save channelId
   */
  if(window.Twitch.ext) {
    window.Twitch.ext.onAuthorized(function(auth) {
      sr.setChannelId(auth.channelId);
    });
  }

  // Don't Open This One
  // function getUserId(name) {
  //   var req = new XMLHttpRequest();
  //   req.open( "GET", "https://api.twitch.tv/helix/users?login=" + name, false);
  //   req.setRequestHeader("Accept", "application/vnd.twitchtv.v5+json");
  //   req.setRequestHeader("Client-ID", "8jefzhtahl455ftq87euflde7ad69m");
  //
  //   req.onreadystatechange = function() {
  //     if (req.readyState == 4 && req.status == 200){
  //       console.log(JSON.parse(req.responseText));
  //     }
  //   }
  //   req.send( null );
  // }

  /**
   * Polls Stream by channel Id to gather information to see what the current user is
   *  playing
   */
  function pollGame(sr){
    var req = new XMLHttpRequest();
    req.open( "GET", sr.streamPath + sr.info.channelId, false);
    req.setRequestHeader("Accept", "application/vnd.twitchtv.v5+json");
    req.setRequestHeader("Client-ID", "8jefzhtahl455ftq87euflde7ad69m");

    req.onreadystatechange = function() {
      if (req.readyState == 4 && req.status == 200){
        var res = JSON.parse(req.responseText);

        if(res.stream) {
          sr.info.isLive = true;
          $('.jiffy-offline').addClass('hidden');
          $('.jiffy-input').removeClass('hidden');

          var strippedName = res.stream.game.replace(/[.,\/#!$%\^&\*\ ;:{}=\-_`~()]/g,"").toLowerCase();
          var strippedSaveName = sr.info.game.replace(/[.,\/#!$%\^&\*\ ;:{}=\-_`~()]/g,"").toLowerCase();

          if(strippedName != strippedSaveName){
            sr.info.game = res.stream.game;
            getGameInfo(sr);
          }
        } else {
          // sr.info.isLive = false;
          // $('.jiffy-input').addClass('hidden');
          // $('.jiffy-offline').removeClass('hidden');
        }
      }
    }
    req.send( null );
  }

  /**
   * Grabs records for the current game & the current user's personal bests
   */
  function pollRecords(sr){
    var queryVars = "",
        response = function(responseText){
          var res = JSON.parse(responseText);
          
          var update = false;
          for(var indx = 0; indx < 3; indx++){
            if(!sr.viewerInfo.runs[indx] ||
                (res.data.runs[indx].run && 
                 sr.viewerInfo.runs[indx].time != res.data.runs[indx].run.times.primary_t) ) {

              var playerSize = res.data.runs[indx].run.players.length,
                  currentRun = res.data.runs[indx].run,
                  insertRun = {
                    "players": [],
                    "date": currentRun.date,
                    "time": currentRun.times.primary_t,
                    "place": res.data.runs[indx].place
                  };
  
              for(var playerIndx = 0; playerIndx < playerSize; playerIndx++){
                var currentPlayer = res.data.players.data[(indx * playerSize) + playerIndx],
                    insertPlayer = {};
                insertPlayer["name"] = currentPlayer.names.international
                insertPlayer["rel"] = currentPlayer.rel
                insertRun.players.push(insertPlayer);
              }
              sr.viewerInfo.runs[indx] = insertRun;
              update = true;
            }
          }
  
          if(res.data.runs.length < 3){
            for(var endIndx = res.data.runs.length; endIndx < 3; endIndx++){
              sr.viewerInfo.runs[endIndx] = "";
            }
          }
  
          if(update){
            //Set new UniqueId to inform client of new Info
            sr.updateUniqueId();
          }
        }
    
    for(var id in sr.info.subcategories) {
      if(id != 0){
        queryVars += "&";
      }
      queryVars += "var-" + id + "=" + sr.info.subcategories[id];
    }

    if( sr.info.individualLevels ) {
      sr.client.get(sr.srPath + "leaderboards/" + sr.gameInfo.id +
                                "/level/" + sr.info.levelId +
                                "/" + sr.info.categoryId +
                                "?top=3&embed=players&" + queryVars,
                                response);
    // /leaderboards/{game}/level/{level}/{category}
    } else {
      sr.client.get(sr.srPath + "leaderboards/" + sr.gameInfo.id +
                                "/category/" + sr.info.categoryId +
                                "?top=3&embed=players&" + queryVars,
                                response);
    }


    //Gather current user's personal bests
    sr.client.get(sr.srPath + "users/" + sr.info.userId +
                              "/personal-bests", function(responseText){
      var res = JSON.parse(responseText),
          found = false,
          updated = false;
      for(var index = 0; index < res.data.length; index++){
        var currentRun = res.data[index];

        if(sr.info.hasVariable){
          if(Object.keys(currentRun.run.values).length > 0){

            for(var key in currentRun.run.values){
              if(currentRun.run.category == sr.info.categoryId){
                for(var subcat in sr.info.subcategories){
                  if(currentRun.run.values[key] == sr.info.subcategories[subcat]) {
                    found = true;
                    if(currentRun.run.times.primary_t != sr.viewerInfo.runnerRecord.time) {
                      updated = true;
                      sr.viewerInfo["runnerRecord"] = {
                        "place": currentRun.place,
                        "date": currentRun.run.date,
                        "time": currentRun.run.times.primary_t
                      };
                    }
                    break;
                  }
                }

                if(updated){
                  sr.updateUniqueId();
                  break;
                }
              }
            }
          }
        } else {
          if(currentRun.run.category == sr.info.categoryId) {
            found = true;
            if(currentRun.run.times.primary_t != sr.viewerInfo.runnerRecord.time) {
              updated = true;
              sr.viewerInfo["runnerRecord"] = {
                "place": currentRun.place,
                "date": currentRun.run.date,
                "time": currentRun.run.times.primary_t
              };
            }

            break;
          }
        }
      }

      if(!sr.viewerInfo["runnerRecord"] || !found && !updated) {
        sr.viewerInfo["runnerRecord"] = "none";
      }
    });
  }

  /**
   * Sends viewer information to client's through twitch's helper script
   */
  function sendInfo(sr){
    if(sr.readyToSend()){
      var testSize = JSON.stringify(sr.viewerInfo);
      window.Twitch.ext.send("broadcast", "application/json", JSON.stringify(sr.viewerInfo));
    } else {
      window.Twitch.ext.send("broadcast", "application/json", "off");
    }
  }

  /**
   * Polls SRCom for Username, makes sure it's accurate and saves the ID
   */
  function getRunnerId(sr) {
    sr.viewerInfo.runnerRecord = "none"
    sr.client.get(sr.srPath + "users?lookup=" + sr.info.username, function(responseText){
      var res = JSON.parse(responseText);
      if(res.data.length == 0){
        sr.info.username = "";
        sr.viewerInfo.name = "";

        $('#srUsername').addClass('error');
        $('#srUsername').next().removeClass('hidden');
        $('#srUsername').nextAll('.checkmark').addClass('hidden');
      } else if(res.data.length == 1) {
        $('#srUsername').removeClass('error');
        $('#srUsername').next().addClass('hidden');
        $('#srUsername').nextAll('.checkmark').removeClass('hidden');
        $('#srUsername').val(res.data[0].names.international);

        window.setTimeout(function(){
          $('#srUsername').nextAll('.checkmark').addClass('hidden');
        }, 1500);

        sr.info.userId = res.data[0].id;
        sr.info.username = res.data[0].names.international;
        sr.viewerInfo["name"] = res.data[0].names.international;
        sr.viewerInfo["runnerRecord"] = "none";

        //Set new UniqueId to inform client of new Info
        sr.updateUniqueId();
      } else {
        sr.info.username = "";
        sr.viewerInfo.name = "";

        $('#srUsername').addClass('error');
        $('#srUsername').next().removeClass('hidden');
      }
    });
  }

  /**
   * Polls SRCom for game Information, sets up category select
   */
  function getGameInfo(sr){
    var encodedName = encodeURI(sr.info.game);
    sr.client.get(sr.srPath + "games?name=" + encodedName +
                    "&embed=categories,variables,levels", function(responseText){
      var res = JSON.parse(responseText);

      //If started Polling SrCom - No harm to repeat
      sr.pausePollSr();

      if(res.data.length == 0){
        $('#srGame').addClass('error');
        $('#srGame').next().removeClass('hidden');
        $('#srGame').nextAll('.checkmark').addClass('hidden');
      } else {
        //Set new UniqueId to inform client of new Info
        sr.updateUniqueId();

        //Save game for use of categories/subcategory selection
        sr.gameInfo = res.data[0];

        //Add game assets/name/weblink to viewerInfo
        sr.viewerInfo["assets"] = [];
        sr.viewerInfo.cover = sr.gameInfo.assets["cover-large"].uri;
        sr.viewerInfo["gameName"] = sr.gameInfo.names.international;
        sr.viewerInfo["weblink"] = sr.gameInfo.weblink;

        //Remove Possibly Set information
        sr.viewerInfo["category"] = "";
        sr.viewerInfo["subcategories"] = [];
        sr.viewerInfo["runs"] = ["", "", ""];
        sr.viewerInfo["runnerRecord"] = "";
        sr.viewerInfo["hasVariable"] = false;
        sr.info.categoryId = "";
        sr.info.subcategories = [];
        sr.info.hasVariable = false;
        sr.info.individualLevels = false;

        $('#srGame').removeClass('error');
        $('.error-game').addClass('hidden');
        $('#srGame').next().addClass('hidden');
        $('#srGame').nextAll('.checkmark').removeClass('hidden');
        window.setTimeout(function(){
          $('#srGame').nextAll('.checkmark').addClass('hidden');
        }, 1500);

        $('.choices').addClass('hidden');
        $('.choices').html();

        sr.info.game = sr.gameInfo.names.international;
        $('#srGame').val(sr.gameInfo.names.international);
        $('.current-game').html("Current Game: " + sr.gameInfo.names.international);

        //Loop through categories to only grab 'per-game' categories
        var categories = sr.gameInfo.categories.data;
            addedOptions = "<option disabled>Select a Category</option>",
            firstFound = false;

        for(var indx = 0; indx < categories.length; indx++) {
          var currentCategory = categories[indx];
          if(currentCategory.type == "per-game" && !firstFound){
            addedOptions += "<option selected value='" + currentCategory.id + "' data-weblink='" + currentCategory.weblink +"'>" + currentCategory.name + "</option>"
            firstFound = true;
            $('.srCategory .text').html('currentCategory.name');
          } else {
            addedOptions += "<option value='" + currentCategory.id + "' data-weblink='" + currentCategory.weblink +"'>" + currentCategory.name + "</option>"
          }
        }
        $("#srCategory").html(addedOptions);
        
        sr.populateVariables();
        //Set Variables for Default Category
      }
    });
  }

  /**
   * If game-data is found in localStorage, repopulates inputs and initial state
   */
  function repopulateGameInfo(sr){
    //Repopulate html from the localStorage info
    $('#srUsername').val(sr.info.username);

    if(sr.info.advanced) {
      $('#srGame').removeAttr('disabled');
      $('.advanced').addClass('active');
      sr.info.isLive = true;
    } else {
      sr.info.isLive = false;
    }

    sr.viewerInfo["name"] = sr.info.username;

    //Check Speedrun.com for game name
    var encodedName = encodeURI(sr.info.game);

    sr.client.get(sr.srPath + "games?name=" + encodedName + 
                    "&embed=categories,variables,levels", function(responseText){
      var res = JSON.parse(responseText);
      if(res.data.length == 0){
        //Results are empty, could not find game
        $('#srGame').addClass('error');
        $('.error-game').removeClass('hidden');
        $("#srCategory, .category-label").addClass('hidden');
      } else {
        var game = res.data[0];

        //Add game assets/name/weblink to viewerInfo
        sr.viewerInfo["cover"] = game.assets["cover-large"].uri;
        sr.viewerInfo["gameName"] = game.names.international;

        //Save game for use of categories/subcategory selection
        sr.gameInfo = game;
        $('#srGame').val(game.names.international);
        $('.current-game').html("Current Game: " + game.names.international);

        //Category Save/Setup
        var categories = game.categories.data,
            levels = game.levels.data,
            variables = game.variables.data,
            addedcategories = "<option disabled selected>Select a Category</option>";

        for(var indx = 0; indx < categories.length; indx++) {
          var currentCategory = categories[indx];

          if(currentCategory.id == sr.info.categoryId) {
            sr.viewerInfo["category"] = currentCategory.name;
            sr.viewerInfo["weblink"] = currentCategory.weblink;

            addedcategories += "<option selected value='" + currentCategory.id + "' data-weblink='" + currentCategory.weblink + "'>" + currentCategory.name + "</option>"
          } else {
            addedcategories += "<option value='" + currentCategory.id + "' data-weblink='" + currentCategory.weblink + "'>" + currentCategory.name + "</option>"
          }
        }
        $("#srCategory").html(addedcategories);
        $("#srCategory, .category-label").removeClass('hidden');


        //Levels Save/Setup
        if(sr.info.individualLevels) {
          var levelOptions = "",
              totalChoices = "",
              choiceCount = 0;
  
          for(var indx = 0; indx < levels.length; indx++){
            var currentLevel = levels[indx]
    
            if(currentLevel.id == sr.info.levelId) {
              levelOptions += "<option selected value='" + currentLevel.id + "'>" + currentLevel.name + "</option>";
              sr.viewerInfo.subcategories[choiceCount] = currentLevel.name;
              sr.viewerInfo.hasVariable = true;
            } else {
              levelOptions += "<option value='" + currentLevel.id + "'>" + currentLevel.name + "</option>";
            }
          }
  
          $('.srLevels, .levels-label').removeClass('hidden');
          $('#srLevels').html(levelOptions);
          choiceCount++;

          for(var indx = 0; indx < variables.length; indx++){
            var currentVar = variables[indx];

            if( currentVar.category == sr.info.categoryId && 
                currentVar.scope.type == "single-level" &&
                currentVar.scope.level == sr.info.levelId && 
                currentVar["is-subcategory"] &&
                currentVar.mandatory){
  
              totalChoices += "<label class='line-label choice-label' for='choice-" + choiceCount +"'> " + currentVar.name + "</label> \
                                <select id='choice-" + choiceCount + "' data-variable='" + currentVar.id + "' class='choice-select'>";
    
              totalChoices += "<option disabled>Select a subcategory</option>";

              for(var currentId in currentVar.values.values){
                var currentSubcategory = currentVar.values.values[currentId],
                    found = false;
  
                for(var subcat in sr.info.subcategories){
                  if(sr.info.subcategories[subcat] == currentId){
                    found = true;
                    break;
                  }
                }
                
                if(found) {
                  totalChoices += "<option selected value='" + currentId + "'>" + currentSubcategory.label + "</option>";
                  sr.viewerInfo.subcategories[choiceCount] = currentSubcategory.label;
                } else {
                  totalChoices += "<option value='" + currentId + "'>" + currentSubcategory.label + "</option>";
                }
              }
    
              totalChoices += "</select>"
              choiceCount++;
            }
          }
  
          $('.choices').removeClass('hidden');
          $('.choices').html(totalChoices);
          hookSubcategories();
        } else {
          
          //Subcategory Save/Setup
          var variables = sr.gameInfo.variables.data,
              resFunction = function(responseText) {

                var res = JSON.parse(responseText);

                for(var runIndx = 0; runIndx < 3; runIndx++){
                  var currentRun = res.data.runs[runIndx].run,
                      insertRun = {"players": []};

                  if(currentRun){
                    insertRun["date"] = currentRun.date;
                    insertRun["time"] = currentRun.times.primary_t;
                    insertRun["place"] = res.data.runs[runIndx].place;

                    var playerSize = res.data.runs[runIndx].run.players.length;
                    
                    for(var playerIndx = 0; playerIndx < playerSize; playerIndx++){
                      var currentPlayer = res.data.players.data[(runIndx * playerSize) + playerIndx],
                          insertPlayer = {};
                      insertPlayer["name"] = currentPlayer.names.international
                      insertPlayer["rel"] = currentPlayer.rel
                      insertRun.players.push(insertPlayer);
                    }
                    sr.viewerInfo.runs[runIndx] = insertRun;
                  } else {
                    sr.viewerInfo.runs[runIndx] = "";
                  }
                }
              }

          if(variables.length > 0){
            //var totalOptions = "<option disabled selected>Select a Variable</option>";
            var first = true,
                addedSubcategories = "",
                subcategoryIds = {};

            for(var indx = 0; indx < variables.length; indx++){
              var currentVar = variables[indx];
              if((currentVar.category == sr.info.categoryId || !currentVar.category) &&
                  currentVar["is-subcategory"] &&
                  currentVar.mandatory){
                if(first){
                  sr.info.hasVariable = true;
                  sr.viewerInfo["hasVariable"] = true;
                  sr.viewerInfo["subcategories"] = [];
                  first = false;
                }

                addedSubcategories += "<label class='line-label choice-label' for='choice-" + indx +"'> " + currentVar.name + "</label> \
                                      <select id='choice-" + indx + "' data-variable='" + currentVar.id + "' class='choice-select'>";

                addedSubcategories += "<option disabled selected>Select a subcategory</option>";
                for(var currentId in currentVar.values.values){
                  var currentSubcategory = currentVar.values.values[currentId];

                  var found = false;
                  for(var subcatIndx in sr.info.subcategories){
                    if(sr.info.subcategories[subcatIndx] == currentId){
                      found = true;
                      break;
                    }
                  }
                  if(found){
                    addedSubcategories += "<option selected value='" + currentId + "'>" + currentSubcategory.label + "</option>";
                    sr.viewerInfo.subcategories.push(currentSubcategory.label);
                    subcategoryIds[currentVar.id] = currentId;
                  } else {
                    addedSubcategories += "<option value='" + currentId + "'>" + currentSubcategory.label + "</option>";
                  }
                }

                addedSubcategories += "</select>"


                // totalOptions += "<option value='" + currentVar.id + "'>" + currentVar.name + "</option>";
                // hasVariable = true;
                // subcategories.push(currentVar);
              }
            }

            $('.choices').html(addedSubcategories);
            $('.choices').removeClass('hidden');
            hookSubcategories();

            if(Object.keys(subcategoryIds).length > 0) {
              var queryVars = "";
              for(var id in subcategoryIds) {
                if(id != 0){
                  queryVars += "&";
                }
                queryVars += "var-" + id + "=" + subcategoryIds[id];
              }

              //Getting leaderboards w/ Subcategories
              sr.client.get(sr.srPath + "leaderboards/" +
                            sr.gameInfo.id + "/category/" +
                            sr.info.categoryId + "?top=3&embed=players&" + queryVars, resFunction);
            } else {
              sr.client.get(sr.srPath + "leaderboards/" +
                            sr.gameInfo.id + "/category/" +
                            sr.info.categoryId + "?top=3&embed=players", resFunction);
            }
          } else {
            //Getting leaderboards w/No Subcategories
            sr.client.get(sr.srPath + "leaderboards/" +
                          sr.gameInfo.id + "/category/" +
                          sr.info.categoryId + "?top=3&embed=players", resFunction);
          }
        }

      }
    });

    sr.client.get(sr.srPath + "users/" + sr.info.userId + "/personal-bests", function(responseText){
      var res = JSON.parse(responseText);
      for(var index = 0; index < res.data.length; index++){
        var currentRun = res.data[index];

        if(sr.info.hasVariable){
          if(Object.keys(currentRun.run.values).length > 0){
            for(var key in currentRun.run.values){
              if(currentRun.run.category == sr.info.categoryId && currentRun.run.values[key] == sr.info.subcategoryId) {
                sr.viewerInfo["runnerRecord"] = {
                  "time": currentRun.times.primary_t,
                  "date": currentRun.date,
                  "place": currentRun.place
                };
                break;
              }
            }
          }

        } else {
          if(currentRun.run.category == sr.info.categoryId) {
            sr.viewerInfo["runnerRecord"] = {
              "time": currentRun.times.primary_t,
              "date": currentRun.date,
              "place": currentRun.place
            };
            break;
          }
        }
      }

      if(!sr.viewerInfo["runnerRecord"]) {
        sr.viewerInfo["runnerRecord"] = "none";
      }
    });

    if(sr.readyToPollSr()) {
      sr.startPollSr();
    }

    sr.startSend();
  }

  /** 
   * Use when re-adding subcategories, re-hook the on-change, and dropdown
   *    the new selects
   */
  function hookSubcategories(){
    $('.choice-select').each(function(index){
      $(this).dropdown({
        placeholder: 'Select a Subcategory'
      });
    });

    $('.choice-select').on('change', function(e){
      $(this).removeClass('error flash');

      var index = $('.choice-select').index(this),
          varId = $(this).find('select').attr('data-variable'),
          selectedId = $('.choice-select').eq(index).find('option:selected').attr('value');

      if(sr.info.levelId != ""){
        sr.viewerInfo.subcategories[index+1] = $('.choice-select').eq(index).find('option:selected').text();
      } else {
        sr.viewerInfo.subcategories[index] = $('.choice-select').eq(index).find('option:selected').text();
      }
      
      sr.info.subcategories[varId] =  selectedId;

      if(sr.readyToPollSr()){
        //Set new UniqueId to inform client of new Info
        sr.updateUniqueId();

        sr.startPollSr();
      }
    });

  };
});
