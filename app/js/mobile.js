/*

  Set Javascript specific to the extension viewer view in this file.

*/

class JiffyClient {
  constructor() {
    this.clientInfo = "";
    this.clientTimer = 0;
    this.clientOn = false;
  }

  updateLeaderboard(){

    $('.broadcaster-row').removeClass('hidden');
    var topPlace = false;
    //Setup leaderboard name/times
    for(var runIndx = 0; runIndx < 3; runIndx++) {
      var currentRun = this.clientInfo.runs[runIndx];
      //check !null for sub 3 leaderboards
      if(currentRun){
        var name = "";
        if(currentRun.players[0].rel == "user"){
          name = currentRun.players[0].name;
        } else {
          name = currentRun.players[0].name.substring(4);
        }

        if(currentRun.players.length > 1){
          name += " +" + (currentRun.players.length - 1);
        }

        $('.runner-name').eq(runIndx).html(name);

        if(!currentRun.place)
          $('.rank').eq(runIndx).html("<div class='circle'></div>-" );
        else
          $('.rank').eq(runIndx).html("<div class='circle'></div>" + this.formatRank(currentRun.place) );

        $('.run-time').eq(runIndx).html( this.formatTime(currentRun.time) );
        $('.run-date').eq(runIndx).html( this.formatDate(currentRun.date) );
        for(var playerIndx in currentRun.players) {
          if(currentRun.players[playerIndx].rel == "user" &&
             currentRun.players[playerIndx].name == this.clientInfo.name) {
            $('.rank').eq(runIndx).find('.circle').addClass('active');
            topPlace = true;
            break;
          }
        }
      } else {
        $('.rank').eq(runIndx).html("-");
        $('.runner-name').eq(runIndx).html("-");
        $('.run-time').eq(runIndx).html("-");
        $('.run-date').eq(runIndx).html("-");
      }
    }

    if(topPlace){
      $('.broadcaster-row').addClass('hidden');
    } else {
      $('.broadcaster-row').removeClass('hidden');
      $('.broadcaster-name').html(this.clientInfo.name);

      if(this.clientInfo.runnerRecord != "none") {
        $('.broadcaster-place').html("<div class='circle active'></div>" + this.formatRank(this.clientInfo.runnerRecord.place) );
        $('.broadcaster-time').html( this.formatTime(this.clientInfo.runnerRecord.time) );
        $('.broadcaster-date').html( this.formatDate(this.clientInfo.runnerRecord.date) );
      } else {
        $('.broadcaster-place, .broadcaster-time, .broadcaster-date').html('-');
      }
    }

    $('.game-art').prop('style', "background: url('" + this.clientInfo.cover + "') center/cover no-repeat");
    $('.shown-link').attr('data-link', this.clientInfo.weblink);

    //Setup Game information
    $('.game-name').html(this.clientInfo.gameName);
    $('.category').html(this.clientInfo.category);

    var subcatText = "";
    if(this.clientInfo.hasVariable){
      var subcategories = this.clientInfo.subcategories;
      for(var varIndx in subcategories) {
        subcatText += "<h2 class='subcategory'>" + subcategories[varIndx] +
                      "</h2>";

      }

      $('.subcategories').html(subcatText);
    } else {
      $('.subcategories').html("");
    }
  }

  formatTime(seconds) {
    var hrs = Math.floor(seconds / 3600);
    var minutes = Math.abs((hrs * 60) - Math.floor(seconds / 60));
    var strSeconds = Math.floor(seconds % 60);
    var milliseconds = Math.round((seconds % 1) * 1000);

    if(hrs == 0) {
      hrs = "";
    } else {
      hrs = hrs + "h ";
    }

    if(minutes == 0){
      minutes = ""
    } else {
      if(minutes < 10) {
        minutes = "0" + minutes;
      }
      minutes = minutes + "m ";
    }

    if(strSeconds < 10) {
      strSeconds = "0" + strSeconds;
    }

    if(milliseconds == 0) {
      milliseconds = "";
    } else {
      if(milliseconds < 10) {
        minutes = "00" + minutes;
      } else if(milliseconds < 100){
        minutes = "0" + minutes;
      }

      milliseconds = " " + milliseconds + "ms";
    }
    return hrs + minutes + strSeconds + "s" + milliseconds;
  }

  formatRank(rank) {
    switch(rank % 10){
      case 1:
        return rank + "st";
        break;
      case 2:
        return rank + "nd";
        break;
      case 3:
        return rank + "rd";
        break;
      default:
        return rank + "th";
        break;
    }
    return rank;
  }

  formatDate(date) {
    var timeNow = Date.now();
    var dateRun = Date.parse(date);

    var timeDiff = timeNow - dateRun;

    var days = Math.floor(timeDiff / 86400000);
    var months = Math.floor(days / 31);
    var years = Math.floor(months / 12);

    if(years > 0){
      if(years == 1) {
        return years + " year ago";
      } else {
        return years + " years ago";
      }
    } else if (months > 0) {
      if(months == 1){
        return months + " month ago";
      } else {
        return months + " months ago";
      }
    } else if (days > 0) {
      if(days == 1) {
        return days + " day ago";
      } else {
        return days + " days ago";
      }
    } else {
      return "Today";
    }
  }
}

$(document).ready(() => {

  var jClient = new JiffyClient();
  var popupTimer;

  if(window.Twitch.ext) {
    window.Twitch.ext.listen("broadcast", broadcastListen);
  }

  $('.shown-link').on('click', function(e) {
    e.preventDefault();
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($('.shown-link').attr('data-link')).select();
    document.execCommand("copy");
    $temp.remove();

    popup('.shown-link');
  })

  $('.nice-link').on('click', function(e) {
    e.preventDefault();
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($('.nice-link').attr('data-link')).select();
    document.execCommand("copy");
    $temp.remove();

    // $('.nice-link .popup').addClass('active');
    popup('.nice-link');

  })

  function popup(selector) {
    var position = $(selector).position();
    var width = $(selector).width();
    var height = $(selector).height();
    var popupHeight = $('.popup').outerHeight();

    $(".popup").attr('style', 'top: ' + (position.top - popupHeight) +'px; left: ' + (position.left + (width/2)) + 'px;');
    $(".popup").addClass('active');

    window.clearTimeout(popupTimer);
    popupTimer = window.setTimeout(function(){
      $(".popup").removeClass('active');
    }, 2000);
  }

  function broadcastListen(target, contentType, message){
    if(message == "off" || message == ""){
      $('.overlay, .underlay').removeClass('active');
      $('.bottom, .footer').addClass('hidden');
      $('.top-half .game-name').html("SRC Miniboard Offline - Waiting for info");
    } else {
      jClient.clientTimer = Date.now();
      var jMsg = JSON.parse(message);
      // $('.offline').removeClass('active');
      $('.bottom, .footer').removeClass('hidden');

      if(!jClient.clientInfo || jMsg.uniqueId != jClient.clientInfo.uniqueId){
        jClient.clientInfo = jMsg;
        jClient.updateLeaderboard();
      }
    }
  }

});
