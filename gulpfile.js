var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify-es').default;
var concat = require('gulp-concat');
var pkg = require('./package.json');
var pump = require('pump');

// Set the banner content
var banner = ['/*!\n',
  ' * <%= pkg.title %> v<%= pkg.version %> \n',
  ' */\n',
  ''
].join('');

// Compiles SCSS files from /scss into /css
gulp.task('sass', function() {
  pump([
    gulp.src('app/scss/*.scss'),
    sass(),
    header(banner, {
      pkg: pkg
    }),
    gulp.dest('app/css'),
    browserSync.reload({
      stream: true
    })
  ])
});

// Minify compiled CSS
gulp.task('minify-css', ['sass'], function() {
  pump([
    gulp.src(['app/css/**/*.css', '!app/css/**/*.min.css']),
    cleanCSS({
      compatibility: 'ie10'
    }),
    rename({
      suffix: '.min'
    }),
    gulp.dest('app/css'),
    browserSync.reload({
      stream: true
    })
  ])
});

gulp.task('concat-css', ['minify-css'], function() {
  return gulp.src(['app/css/*.min.css', '!app/css/style.min.css'])
    .pipe(cleanCSS({
      compatibility: 'ie10'
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// Minify custom JS
gulp.task('minify-js', function() {
  var options = {};

  pump([
    gulp.src(['app/js/**/*.js', '!app/js/**/*.min.js']),
    uglify(options),
    header(banner, {
      pkg: pkg
    }),
    rename({
      suffix: '.min'
    }),
    gulp.dest('app/js'),
    browserSync.reload({
      stream: true
    })
  ], function(err) {
    if(err)
      console.log('-- Minify-JS Err -- :', err);
    else
      console.log("-- Minify-JS Finished --")
  })
});

gulp.task('output-html', function(){
  var options = {};

  pump([
    gulp.src(['app/*.html']),
    gulp.dest('app/output'),
  ], function(err) {
    if(err)
      console.log('-- Output-HTML Err -- :', err);
    else
      console.log("-- Output-HTML Finished --")
  })
});

gulp.task('output-images', function(){
  var options = {};

  pump([
    gulp.src(['app/images/*']),
    gulp.dest('app/output/images'),
  ], function(err) {
    if(err)
      console.log('-- Output-Images Err -- :', err);
    else
      console.log("-- Output-Images Finished --")
  })
});

gulp.task('output-css', ['minify-css'], function(){
  var options = {};

  pump([
    gulp.src(['app/css/**/*.min.css']),
    gulp.dest('app/output/css'),
  ], function(err) {
    if(err)
      console.log('-- Output-CSS Err -- :', err);
    else
      console.log("-- Output-CSS Finished --")
  })
});

gulp.task('output-js', ['minify-js'], function(){
  pump([
    gulp.src(['app/js/**/*.min.js']),
    gulp.dest('app/output/js'),
  ], function(err) {
    if(err)
      console.log('-- Output-JS Err -- :', err);
    else
      console.log("-- Output-JS Finished --")
  })
});

gulp.task('output', ['output-html', 'output-images', 'output-js', 'output-css'], function() {

});

// Copy vendor files from /node_modules into /vendor
// NOTE: requires `npm install` before running!
// gulp.task('copy', function() {
//   gulp.src([
//       'node_modules/bootstrap/dist/**/*',
//       '!**/npm.js',
//       '!**/bootstrap-theme.*',
//       '!**/*.map'
//     ])
//     .pipe(gulp.dest('vendor/bootstrap'))

//   gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/jquery/dist/jquery.min.js'])
//     .pipe(gulp.dest('vendor/jquery'))

//   gulp.src(['node_modules/jquery.easing/*.js'])
//     .pipe(gulp.dest('vendor/jquery-easing'))

//   gulp.src([
//       'node_modules/font-awesome/**',
//       '!node_modules/font-awesome/**/*.map',
//       '!node_modules/font-awesome/.npmignore',
//       '!node_modules/font-awesome/*.txt',
//       '!node_modules/font-awesome/*.md',
//       '!node_modules/font-awesome/*.json'
//     ])
//     .pipe(gulp.dest('vendor/font-awesome'))
// })

// Default task
// gulp.task('default', ['sass', 'minify-css', 'minify-js', 'copy', 'concat-css']);
gulp.task('default', ['sass', 'minify-css', 'minify-js']);

// Configure the browserSync task
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'app/',
      https: {
        key: "certs/testing.key",
        cert: "certs/testing.crt"
      }
    },
  })
})

// Dev task with browserSync
gulp.task('dev', ['browserSync', 'sass', 'minify-css', 'minify-js'], function() {
  gulp.watch('app/scss/*.scss', ['sass', 'minify-css']);
  gulp.watch(['app/js/**/*.js', '!app/js/**/*.min.js'], ['minify-js']);
  // gulp.watch('app/css/*.css', ['minify-css', 'concat-css']);
  // Reloads the browser whenever HTML or JS files change
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/*.min.js', browserSync.reload);
});